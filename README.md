Symfony API
=================

A Symfony project created on February 6, 2017, 11:39 pm.

Requeirements
-------------
* PHP 7.1
* MySQL 5.7
* Node.js >=6
* npm, bower, gulp, sass
* Composer
* Supports AngularJS

Installation
------------
* Install libraries:
```
    composer install
```
* Build assets:
```
    npm i
    bower i
    gulp
```
* Create database, execute migrations and fixtures:
```
    php bin/console app:db:reload
```

Demo credentials
----------------
Users:
* Username: `demo`, Password: `demo` has 3 times for search
* Username: `admin`, Password: `admin` has 5 times for search